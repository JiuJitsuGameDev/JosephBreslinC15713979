class Clouds
{

PVector pos;
float distance;
float radius1;
float radius2;
float reset;
float end;
float speed;
float yRandom;

  Clouds(float x)
  {
    distance = 60;
    yRandom = random(height*.1, height*.4);
    pos = new PVector(x, yRandom);
    radius1 = random( 30, 50);
     radius2 = random( 40, 70);
     reset = width + 100;
     end = -100;
     speed = random(2,4);
     
  } 

  void displayClouds()
  {
    fill(255, 180);
    noStroke();
    ellipseMode(CENTER);
    ellipse(pos.x, pos.y + 20, radius2,radius2 -5);
     ellipse(pos.x - 20, pos.y, radius1,radius1 -5);
     ellipse(pos.x +20, pos.y, radius2,radius2 -5);
       ellipse(pos.x, pos.y, radius1,radius1 -5);
  }
  
  void moveClouds()
  {
  if(pos.x > end)
  {
  pos.x -= speed;
  }
  if(pos.x< end)
  {
    pos.x = reset;
  }
 
  } 
    
  }
  


