class Plane
{
  PVector pos;
  float reset;
  float end;
  float pitch;
  color c;
  float pLength;
  float pHeight;
  float wWidth;
  float wSpan;
  float x;
  float y;
  float planeStart;
  float planeEnd;
  float speed;
  




  Plane() 
  {
    speed = 3;

    planeStart = -120;
    planeEnd = width + 120;
    x = planeStart;
    y = height *.2;
    pos = new PVector(x, y);
    reset = - 50;
    end = width + 50;
    pitch = 100;
    c = color(180); 
    pLength = 80;
    pHeight = 20;
    wWidth = 10;
    wSpan = 80;
  }

  void displayPlane()
  {
    fill(c);
    noStroke();
    rect(pos.x, pos.y, pLength, pHeight);
    rect(pos.x + pLength/2, pos.y -pHeight *1.5, wWidth, wSpan);
    triangle(pos.x, pos.y, pos.x, pos.y - pHeight, pos.x + pHeight, pos.y);
    triangle(pos.x +pLength, pos.y, pos.x + pLength, pos.y + pHeight, pos.x + pHeight + pLength, pos.y + pHeight);
  }
  void movePlane()
  {
    if (pos.x < planeEnd)
    {
      pos.x += speed;
    }
    if (pos.x> end)
    {
      pos.x = planeStart;
     
    }
  }
}

