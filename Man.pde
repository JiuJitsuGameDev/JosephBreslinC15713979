class Man
{
  PVector headPos;
  PVector hips;
  float head = 5;
  float  bodyLength;
  float armsWidth;
  float armsLength;
  float legLength;
  float stepWidth;

  Man(float x, float y)  
  {
    hips = new PVector(x, y);
    head = 15;
    bodyLength = 50;
    armsWidth = 20;
    armsLength = 35;
    legLength = 35;
    stepWidth = 15;
    headPos = new PVector(hips.x, hips.y - bodyLength);
    }

    void displayMan()
    {
      fill(0);
      stroke(0);
      strokeWeight(4);
      ellipseMode(RADIUS);
      //Head
      ellipse(hips.x, hips.y - bodyLength, head, head);
      //Body
      line(hips.x, headPos.y, hips.x, hips.y);
      //shoulders/arms
      line(hips.x - armsWidth, hips.y - bodyLength + stepWidth, hips.x + armsWidth,  hips.y - bodyLength + stepWidth);
      line(hips.x - armsWidth, hips.y - bodyLength + stepWidth, hips.x - armsWidth,  hips.y - bodyLength + armsLength);
      line(hips.x + armsWidth, hips.y - bodyLength + stepWidth, hips.x + armsWidth,  hips.y - bodyLength + armsLength);
      //Legs 
      line(hips.x, hips.y, hips.x - stepWidth, hips.y + legLength);
      line(hips.x, hips.y, hips.x + stepWidth, hips.y + legLength);
    }
}

