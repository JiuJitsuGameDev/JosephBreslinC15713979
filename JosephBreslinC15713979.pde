Plane plane;
Man man;
Clouds[] clouds;
Pack pack;
float cloudsMove;
PVector manPos;
boolean[] keys = new boolean[2000];
void setup()
{
  size(600, 600);
  manPos = new PVector(50, height * .7);
  cloudsMove = width + 100;
  plane = new Plane();
  man = new Man(manPos.x, manPos.y);
  clouds = new Clouds[4];
  int index = 0;
  for (int i = 0; i < 4; i++) {

    clouds[index++] = new Clouds(cloudsMove);
  }
  pack = new Pack();
}


void background()
{
  noStroke();
  fill(0, 100, 200);
  rect(0, 0, width, height/2);
  fill(0, 255, 100);
  rect(0, height / 2, width, height);
}
void keyPressed()
{
  keys[keyCode] = true;
}

void keyReleased()
{
  keys[keyCode] = true;
}

void draw()
{
  rectMode(CORNER);
  background();
  plane.displayPlane();
  plane.movePlane();
  man.displayMan();
  //
 
  pack.render();
  for (int i = 0; i < 4; i++)
  {
    clouds[i].displayClouds();
    clouds[i].moveClouds();
  }
  
    if (pack.pos.y > pack.stopPack )
      {
        if (man.hips.x < pack.landing && pack.packBool == true)
        {
          man.hips.x ++;
        }
        if (man.hips.y > pack.stopPack && pack.packBool == true)
        {
          man.hips.y --;
        }
      }
        
      

  
  
  
}

